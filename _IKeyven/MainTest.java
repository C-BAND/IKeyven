package _IKeyven;

import java.io.IOException;
import java.util.ArrayList;

public class MainTest {
	public static void main(String[] args) {
		Function.setModel("lib/chinesePCFG.ser.gz",
				"src/_IKeyven/userwords.txt");
		// Function.learn("src/_IKeyven/sentence1.txt",
		// "src/_IKeyven/rules.txt");
		ArrayList<String> list = new ArrayList<>();
		try {
			list = _fileutil.FileUtil
					.readFile("src/_IKeyven/hankcs.txt", "gbk");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (String str : list) {
			System.out.println(str);
			System.out.println(Function.compress(str, "src/_IKeyven/rules.txt",
					false));
			System.out.println(Function.trunkhankey(str,
					"src/_IKeyven/deprules.txt", false));
			System.out.println();
		}
	}
}
