package _fileutil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class FileUtil {
	public static ArrayList<String> readFile(String filePathAndName,
			String codeType) throws IOException {
		File f = new File(filePathAndName);
		if (!f.exists()) {
			_fileutil.Log.log("读取文件失败:该文件不存在!");
			throw new IOException("目录错误!");
		}
		InputStreamReader read = null;
		BufferedReader reader = null;
		ArrayList<String> temp = new ArrayList<String>();
		try {
			if (f.isFile() && f.exists()) {
				read = new InputStreamReader(new FileInputStream(f), codeType);
				reader = new BufferedReader(read);
				String line;
				while ((line = reader.readLine()) != null) {
					temp.add(line);
				}
			}
		} catch (Exception e) {
			_fileutil.Log.log(e.getMessage());
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
				reader = null;
			}
			if (read != null) {
				read.close();
				read = null;
			}
		}
		return temp;
	}

	public static File writeFile(String filePathAndName,
			ArrayList<String> list, String codeType) throws IOException {
		File f = new File(filePathAndName);
		OutputStreamWriter write = null;
		BufferedWriter writer = null;
		try {
			if (!f.exists()) {
				f.createNewFile();
			}
			write = new OutputStreamWriter(new FileOutputStream(f), codeType);
			writer = new BufferedWriter(write);
			for (String str : list) {
				writer.write(str + "\r\n");
			}
		} catch (Exception e) {
			_fileutil.Log.log("写文件内容操作出错");
			_fileutil.Log.log(e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			if (writer != null) {
				writer.close();
				writer = null;
			}
			if (write != null) {
				write.close();
				write = null;
			}
		}
		return f;
	}
}
