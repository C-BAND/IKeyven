package _init;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class StanfordParser {
	public LexicalizedParser lp = null;
	public String model = "";

	private StanfordParser() {
	}

	public void setModel(String model) {
		this.lp = LexicalizedParser.loadModel(model);
		_fileutil.Log.log("�����ķ�:" + model);
	}

	private static class StanfordParserFactory {
		private static StanfordParser instance = new StanfordParser();
	}

	public static StanfordParser getInstance() {
		return StanfordParserFactory.instance;
	}

	public Object readResolve() {
		return getInstance();
	}
}
