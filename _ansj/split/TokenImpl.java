package _ansj.split;

import java.util.List;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

public class TokenImpl implements IToken {
	private String str = "";
	private List<Term> list = null;

	public TokenImpl(String str) {
		list = splitTags(str);
	}

	@Override
	public List<Term> splitTags(String str) {
		// TODO Auto-generated method stub
		this.str = str;
		return list = ToAnalysis.parse(str);
	}

	public void log() {
		// TODO Auto-generated method stub
		_fileutil.Log.log("原句:" + str);
		_fileutil.Log.log("分词:" + list);
	}

	public static void main(String[] args) {
		TokenImpl tl = new TokenImpl("有没有手机？");
		tl.log();
	}

	@Override
	public String getStr() {
		// TODO Auto-generated method stub
		return this.str;
	}

	@Override
	public List<Term> getList() {
		// TODO Auto-generated method stub
		return this.list;
	}
}
